package com.mark.crime;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.support.design.widget.Snackbar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import cn.pedant.SweetAlert.SweetAlertDialog;
import greco.lorenzo.com.lgsnackbar.LGSnackbarManager;
import greco.lorenzo.com.lgsnackbar.style.LGSnackBarTheme;
import greco.lorenzo.com.lgsnackbar.style.LGSnackBarThemeManager;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


import static greco.lorenzo.com.lgsnackbar.style.LGSnackBarTheme.SnackbarStyle.ERROR;


public class MainActivity extends FragmentActivity implements OnMapReadyCallback {
    LinearLayout la,lb,lc;
    private GoogleMap mMap;
    private final static int MY_PERMISION_FINE_LOCATION = 101;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private FusedLocationProviderClient mFusedLocationClient;
    protected Location mLastLocation;
    private String mLatitudeLabel;
    private String mLongitudeLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        LGSnackbarManager.prepare(getApplicationContext(), LGSnackBarThemeManager.LGSnackbarThemeName.SHINE);

        mLatitudeLabel = getResources().getString(R.string.latitude_label);
        mLongitudeLabel = getResources().getString(R.string.longitude_label);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        la = (LinearLayout)findViewById(R.id.lb_a);
        lb = (LinearLayout)findViewById(R.id.lb_b);
        lc = (LinearLayout)findViewById(R.id.lb_c);
        setSelectedItem();

    }

    @Override
    public void onStart() {
        super.onStart();

        if (!checkPermissions()) {
            requestPermissions();
        } else {
            getLastLocation();
        }
    }

    public  void  cmd_category_Clicked(View view){
        ImageButton b = (ImageButton)view;
        variable.category = Integer.valueOf(b.getTag().toString());
        setSelectedItem();

    }
    private void setSelectedItem(){
        int tag =  variable.category;
        if(tag == 1){
            la.setVisibility(View.VISIBLE);
            lb.setVisibility(View.INVISIBLE);
            lc.setVisibility(View.INVISIBLE);
        }else if(tag == 2){
            la.setVisibility(View.INVISIBLE);
            lb.setVisibility(View.VISIBLE);
            lc.setVisibility(View.INVISIBLE);
        }else if(tag == 3){
            la.setVisibility(View.INVISIBLE);
            lb.setVisibility(View.INVISIBLE);
            lc.setVisibility(View.VISIBLE);
        }else{
            la.setVisibility(View.INVISIBLE);
            lb.setVisibility(View.INVISIBLE);
            lc.setVisibility(View.INVISIBLE);
        }
    }
    public  void showError(LGSnackBarTheme.SnackbarStyle style, final String Message){

        LGSnackbarManager.show(style, Message);


    }
    public  void  cmd_go_Clicked(View view){
        if(!tools.isNetworkAvailable(getApplicationContext())){
            showError(ERROR,"No Internet Connection");
        }else{
            GPSTracker gps = new GPSTracker(MainActivity.this);
            if (gps.canGetLocation()) {
                if (variable.category == 0){
                    showError(ERROR,"Invalid Category");
                }else if( variable.address.contains("-") || variable.address == null){
                    showError(ERROR,"Invalid location");
                }else{

                    tools.clearImageFolder(getApplicationContext());
                    startActivity(new Intent(getApplicationContext(),CreateActivity.class));
                    overridePendingTransition(0,0);
                    finish();
                }
            }
        }

    }
    private void dialContactPhone(final String phoneNumber) {
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null)));
    }
    public  void  cmd_makeCall_Clicked(View view){
        dialContactPhone(variable.emergencyNumber);
    }


private  void setMap(){
    mMap.clear();
    variable.address = tools.getCompleteAddressString(MainActivity.this,variable.latittude,variable.longitude);
    LatLng latLng = new LatLng(variable.latittude,variable.longitude);
    mMap.addMarker(new MarkerOptions().position(latLng).title(variable.address));
    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15.0f));
    mMap.getUiSettings().setZoomControlsEnabled(true);
}
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        if(tools.isNetworkAvailable(this)){

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                    setMap();
                }else{

                      Toast.makeText(getApplicationContext(),"Requires Location Permision",Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISION_FINE_LOCATION);
                    }
                }

                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                    @Override
                    public void onMapClick(LatLng latLng) {
                        variable.address = tools.getCompleteAddressString(MainActivity.this,latLng.latitude,latLng.longitude);
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(latLng);
                        markerOptions.title(tools.getCompleteAddressString(MainActivity.this,latLng.latitude,latLng.longitude));
                        mMap.clear();
                        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                        mMap.addMarker(markerOptions);
                    }
                });

                mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        getLastLocation();
                        return false;
                    }
                });

        }else{
            showError(ERROR,"No Internet Connection");
        }

    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            variable.latittude = mLastLocation.getLatitude();
                            variable.longitude = mLastLocation.getLongitude();
                            setMap();
                        } else {
                            Log.w(TAG, "getLastLocation:exception", task.getException());
                            showSnackbar(getString(R.string.no_location_detected));

                        }
                    }
                });
    }
    private void showSnackbar(final String text) {
        View container = findViewById(R.id.splash_activity_container);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }
    private void showSnackbar(final int mainTextStringId, final int actionStringId,View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =   ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);

        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });

        } else {
            Log.i(TAG, "Requesting permission");
            startLocationPermissionRequest();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

    @Override
    public void onBackPressed() {
        //  super.onBackPressed();
        SystemExit();
    }
    private void SystemExit() {
//        AlertDialog.Builder b = new AlertDialog.Builder(this);
//
//        b.setTitle("Confirm exit");
//        b.setMessage("Are you sure?");
//
//        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int which) {
//                // Do do my action here
//
//                finish();
//            }
//
//        });
//        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//
//        AlertDialog alert = b.create();
//        alert.show();
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("Application exit!")
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        MainActivity.this.finish();
                    }
                })
                .show();
    }



}
