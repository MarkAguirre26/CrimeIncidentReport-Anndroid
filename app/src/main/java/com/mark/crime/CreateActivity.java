package com.mark.crime;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class CreateActivity extends AppCompatActivity {
    ArrayList<String> f = new ArrayList<String>();// list of file paths
    File[] listFile;
    EditText txt_message;
    LinearLayout panel_images;
    RelativeLayout main_panel;
    ProgressDialog progressDialog;
    Button cmd_hide_keyBoard;
    String selectedcategory = "";
    public final String TAG = "result";
    Random ran = new Random();

    int randomNum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle(tools.getCategory());
//        LoginManager.getInstance().logOut();
        cmd_hide_keyBoard = (Button)findViewById(R.id.cmd_hide_keyBoard);
        panel_images = (LinearLayout)findViewById(R.id.panel_images);
        main_panel  = (RelativeLayout)findViewById(R.id.main_panel);
        txt_message = (EditText)findViewById(R.id.txt_message);
        randomNum = ran.nextInt(5000) + 5;
        keyBoardVisibility();
        getImages();
        progressDialog  = new ProgressDialog(this);
        progressDialog.setTitle("Sending...");
        switch (variable.category){
            case 1:
                selectedcategory = "Crime";
                break;
            case 2:
                selectedcategory = "Medical";
                break;
            case  3:
                selectedcategory = "Fire";
                break;


        }



    }

    private void getImages() {

        File file= new File(android.os.Environment.getExternalStorageDirectory(),getString(R.string.app_name));
        if(file.exists()) {
            Log.d("Filename", file + "");
            if (file.isDirectory()) {
                listFile = file.listFiles();

                for (int i = 0; i < listFile.length; i++) {
                    String filename = listFile[i].getAbsolutePath();
//                    f.add(filename);
                    Bitmap d = BitmapFactory.decodeFile(filename);
                    int newHeight = (int) (d.getHeight() * (333.0 / d.getWidth()));
                    Bitmap putImage = Bitmap.createScaledBitmap(d, 333, newHeight, true);

                    final ImageView imageView = new ImageView(this);
                    imageView.setImageBitmap(putImage);
                    imageView.setTag(filename);
                    imageView.setOnClickListener(getOnClickDoSomething(imageView));
                    panel_images.addView(imageView);
                }
            }
        }


    }
    View.OnClickListener getOnClickDoSomething(final ImageView img)  {
        return new View.OnClickListener() {
            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(),img.getTag().toString(),Toast.LENGTH_SHORT).show();
                variable.imageTag = img.getTag().toString();
                startActivity(new Intent(getApplicationContext(), ZoomImageActivity.class));
                overridePendingTransition(0, 0);
            }
        };
    }
    private  void  keyBoardVisibility(){
        main_panel.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                main_panel.getWindowVisibleDisplayFrame(r);
                int screenHeight = main_panel.getRootView().getHeight();
                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;
                Log.d(TAG, "keypadHeight = " + keypadHeight);
                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    cmd_hide_keyBoard.setVisibility(View.VISIBLE);
                }
                else {
                    // keyboard is closed
                    cmd_hide_keyBoard.setVisibility(View.GONE);
                }
            }
        });
    }



    public  void cmd_send_Clicked(View view){
        Button b  = (Button)view;
        int tag = Integer.valueOf(b.getTag().toString());

        switch (tag){
            case 1://Hide keyboard
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                break;
            case 2: //Take a Picture
                // Launches camera in Vertical Merge Mode (Captured image will be long)
                tools.clearImageFolder(getApplicationContext());
                startActivity(new Intent(getApplicationContext(),CameraApi.class));
                overridePendingTransition(0,0);
                finish();
                // Launches Camea in Horizontal Merge Mode (Captured image will be wide)
                //LongImageCameraActivity.launch( myActivity, LongImageCameraActivity.ImageMergeMode.HORIZONTAL );
                break;
            case 3: //Send
                sendReport();
                break;
            default://Nothing
        }


    }


    private  void sendReport(){
        progressDialog.show();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url.sendReport,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                upLoadImage();
                                back();
                                progressDialog.dismiss();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("err", error.getMessage());
                        back();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("Subject", selectedcategory);
                        params.put("Description", txt_message.getText().toString());
                        params.put("Lat", variable.latittude+"");
                        params.put("Lng", variable.longitude+"");
                        params.put("Media_code", randomNum+"");
                        params.put("Sender_fbid", registrationData.userId);
                        params.put("ContactNo", variable.emergencyNumber);
                        return params;
                    }
                };
                MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

            }
        };
        new Thread(runnable).start();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        back();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                back();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void back() {
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
        overridePendingTransition(0,0);
        finish();
    }



    public void upLoadImage() {
        for (int i = 0; i < listFile.length; i++) {
            final String filename = listFile[i].getAbsolutePath();
            final String trimeFilename = filename.substring(filename.lastIndexOf("/")+1, filename.length());
            Log.d("fname",trimeFilename);//
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url.uploadPhoto,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.e("INSERT_PHOTO", response);
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("CAMERA_ERROR", error.getMessage());
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put("image", tools.getStringImage(BitmapFactory.decodeFile(filename)));
                            params.put("file_name",trimeFilename);
                            return params;
                        }
                    };
                    MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

                }
            };
            new Thread(runnable).start();
        }



    }
}
